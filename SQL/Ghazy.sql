CREATE OR REPLACE FUNCTION userFollow(uid1 bigint, uid2 bigint) RETURNS VOID AS
$$
BEGIN
    INSERT INTO subscription VALUES (uid1, uid2, '1');
END
$$
  LANGUAGE 'plpgsql';


---------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION userSubscribe(uid1 bigint, uid2 bigint) RETURNS VOID AS
$$
BEGIN
    INSERT INTO subscription VALUES (uid1, uid2, '0');
END
$$
  LANGUAGE 'plpgsql';


---------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION userBlock(uid1 bigint, uid2 bigint) RETURNS VOID AS
$$
BEGIN
    INSERT INTO blocks VALUES (uid1, uid2);
END
$$
  LANGUAGE 'plpgsql';


--------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION getBlocked(uid1 bigint, uid2 bigint) RETURNS table (id bigint)
    LANGUAGE plpgsql
    AS $$
  BEGIN
		return Query
		select exists(select * from blocks where "user1" = uid1 and "user2" = uid2);
	END;
    $$;
