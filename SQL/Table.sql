CREATE TABLE IF NOT EXISTS users (
    uid bigserial NOT NULL primary key,
    email varchar(40) unique NOT NULL,
    pass varchar(20) NOT NULL,
    username varchar(18) unique NOT NULL,
    firstname varchar(20) NOT NULL,
    lastname varchar(20) NOT NULL,
    birthdate date NOT NULL,
    image text,
    description varchar(100),
    isadmin bit NOT NULL
);

CREATE TABLE IF NOT EXISTS blocks (
    user1 bigint NOT NULL references users,
    user2 bigint NOT NULL references users
);

CREATE TABLE IF NOT EXISTS reports (
    user1 bigint NOT NULL references users,
    user2 bigint NOT NULL references users
);

CREATE TABLE IF NOT EXISTS subscription (
    user1 bigint NOT NULL references users,
    user2 bigint NOT NULL references users,
    subtype bit NOT NULL		--0:subscribe , 1:follow
);
