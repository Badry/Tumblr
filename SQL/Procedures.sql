CREATE OR REPLACE FUNCTION getuserbymail(mail varchar) RETURNS users
    AS $$
	declare u users;
    BEGIN

		SELECT * into u FROM users WHERE "email" = mail;
		return u;
	END;
    $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION getUserByID(i bigint) RETURNS users
    LANGUAGE plpgsql
    AS $$
	declare u users;
    BEGIN
		SELECT * into u FROM users WHERE "uid" = i;
		return u;
	END;
    $$;

CREATE OR REPLACE FUNCTION searchUser(usernameString VARCHAR(18)) RETURNS table (id bigint, usern varchar) AS $$
    BEGIN
        return Query
        SELECT uid, username FROM users
        WHERE username ~* usernameString;
    END;
    $$  LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION getFollowers(i bigint) RETURNS table (id bigint)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return Query
        SELECT user1 FROM subscription WHERE "user2" = i AND "subtype" = '1';
    END;
    $$;

CREATE OR REPLACE FUNCTION getSubscribers(i bigint) RETURNS table (id bigint)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return Query
        SELECT user1 FROM subscription WHERE "user2" = i AND "subtype" = '0';
    END;
    $$;

CREATE OR REPLACE FUNCTION getFollows(i bigint) RETURNS table (id bigint)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return Query
        SELECT user2 FROM subscription WHERE "user1" = i AND "subtype" = '1';
    END;
    $$;

CREATE OR REPLACE FUNCTION getSubscribes(i bigint) RETURNS table (id bigint)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return Query
        SELECT user2 FROM subscription WHERE "user1" = i AND "subtype" = '0';
    END;
    $$;

CREATE OR REPLACE FUNCTION userFollow(uid1 bigint, uid2 bigint) RETURNS VOID AS
    $$
    BEGIN
        INSERT INTO subscription VALUES (uid1, uid2, '1');
    END
    $$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION userSubscribe(uid1 bigint, uid2 bigint) RETURNS VOID AS
    $$
    BEGIN
        INSERT INTO subscription VALUES (uid1, uid2, '0');
    END
    $$
  LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION userBlock(uid1 bigint, uid2 bigint) RETURNS VOID AS
    $$
    BEGIN
        INSERT INTO blocks VALUES (uid1, uid2);
    END
    $$
  LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION getBlocked(uid1 bigint, uid2 bigint) RETURNS table (id bigint)
    LANGUAGE plpgsql
    AS $$
    BEGIN
		return Query
		select exists(select * from blocks where "user1" = uid1 and "user2" = uid2);
	END;
    $$;


CREATE or Replace FUNCTION Unblock (userID1 bigint, userID2 bigint ) Returns void
    AS $$
    begin
        DELETE FROM blocks 
        WHERE user1 = userID1 AND user2 = userID2;
    end;
    $$
    Language plpgsql;


