-- DROP TABLE blocks;
-- DROP TABLE reports;
-- DROP TABLE subscription;
-- DROP TABLE users;

CREATE TABLE IF NOT EXISTS users (
    uid bigserial NOT NULL primary key,
    email varchar(40) unique NOT NULL,
    pass varchar(20) NOT NULL,
    username varchar(18) unique NOT NULL,
    firstname varchar(20) NOT NULL,
    lastname varchar(20) NOT NULL,
    birthdate date NOT NULL,
    image text,
    description varchar(100),
    isadmin bit NOT NULL
);


CREATE TABLE IF NOT EXISTS blocks (
    user1 bigint NOT NULL references users,
    user2 bigint NOT NULL references users
);


CREATE TABLE IF NOT EXISTS reports (
    user1 bigint NOT NULL references users,
    user2 bigint NOT NULL references users
);

CREATE TABLE IF NOT EXISTS subscription (
    user1 bigint NOT NULL references users,
    user2 bigint NOT NULL references users,
    subtype bit NOT NULL		--0:subscribe , 1:follow
);


--  DROP FUNCTION getUserByMail(varchar);


CREATE OR REPLACE FUNCTION getuserbymail(mail varchar) RETURNS users
    AS $$
	declare u users;
    BEGIN
 	
		SELECT * into u FROM users WHERE "email" = mail;
		return u;
	END;
    $$ LANGUAGE plpgsql;
	

-- DROP FUNCTION getUserByID(bigint);





CREATE OR REPLACE FUNCTION getUserByID(i bigint) RETURNS users
    LANGUAGE plpgsql
    AS $$
	declare u users;
    BEGIN
		SELECT * into u FROM users WHERE "uid" = i;
		return u;
	END;
    $$;





-- SELECT getUserByMail('mina@gmail');
--  SELECT getuserbyID(1);











