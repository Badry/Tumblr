CREATE OR REPLACE FUNCTION userUnsubscribe(uid1 bigint, uid2 bigint) RETURNS VOID AS
$$
BEGIN
    DELETE FROM subscription 
	WHERE user1 = uid1 AND user2 = uid2 AND subtype = '0';
END
$$
  LANGUAGE 'plpgsql';
  
  
CREATE OR REPLACE FUNCTION userUnfollow(uid1 bigint, uid2 bigint) RETURNS VOID AS
$$
BEGIN
    DELETE FROM subscription 
	WHERE user1 = uid1 AND user2 = uid2 AND subtype = '1';
END
$$
  LANGUAGE 'plpgsql';
  
