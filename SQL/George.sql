--returns true if user is inserted, false otherwise
CREATE OR REPLACE FUNCTION insertUser(mail VARCHAR(40), password VARCHAR(20), uname VARCHAR(18), fname VARCHAR(20), lname VARCHAR(20), bdate DATE, img TEXT, descr VARCHAR(100), isAdmin BIT) RETURNS BOOLEAN AS $$
	DECLARE
	myCount bigint;
	BEGIN
		SELECT COUNT(*) into myCount
		FROM users
		WHERE email = mail OR username = uname;
		IF myCount >0 THEN
    	RETURN FALSE;
		END IF;
		INSERT INTO users (email,pass,username,firstname,lastname,birthdate,image,description,isadmin) VALUES (mail, password, uname, fname, lname, bdate, img, descr, isAdmin);	
		RETURN TRUE;
    END;
    $$ LANGUAGE plpgsql;
--SELECT insertUser('abc0@ex.com','password123','usern0','george','maged','1994-12-12','asd.jpg','desc','0')

--returns true if the uid exists and the row was updated
CREATE OR REPLACE FUNCTION updateUserGivenID(uniqueID bigint, mail VARCHAR(40), password VARCHAR(20), uname VARCHAR(18), fname VARCHAR(20), lname VARCHAR(20), bdate DATE, img TEXT, descr VARCHAR(100), admin BIT) RETURNS BOOLEAN AS $$
	BEGIN
	IF EXISTS (SELECT 1 FROM users WHERE uid = uniqueID)  THEN
		UPDATE users SET email = mail, pass = password, username = uname, firstname = fname, lastname = lname, birthdate = bdate, image = img, description = descr, isadmin = admin
		WHERE uid = uniqueID;
		RETURN TRUE;
		END IF;
		RETURN FALSE;
    END;
    $$ LANGUAGE plpgsql;
--SELECT updateUserGivenID(1,'abcs@ex.com','password123','usernii','george','maged','1994-12-12','asd.jpg','desc','0')


