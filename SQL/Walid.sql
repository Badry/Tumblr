﻿
-- DROP FUNCTION getFollowers(bigint);

CREATE OR REPLACE FUNCTION getFollowers(i bigint) RETURNS table (id bigint)
    LANGUAGE plpgsql
    AS $$
    BEGIN
		return Query
		SELECT user1 FROM subscription WHERE "user2" = i AND "subtype" = '1';
		
	END;
    $$;

-- DROP FUNCTION getSubscribers(bigint);

CREATE OR REPLACE FUNCTION getSubscribers(i bigint) RETURNS table (id bigint)
    LANGUAGE plpgsql
    AS $$
    BEGIN
		return Query
		SELECT user1 FROM subscription WHERE "user2" = i AND "subtype" = '0';
		
	END;
    $$;


-- DROP FUNCTION getFollows(bigint);

CREATE OR REPLACE FUNCTION getFollows(i bigint) RETURNS table (id bigint)
    LANGUAGE plpgsql
    AS $$
    BEGIN
		return Query
		SELECT user2 FROM subscription WHERE "user1" = i AND "subtype" = '1';
		
	END;
    $$;


-- DROP FUNCTION getSubscribes(bigint);

CREATE OR REPLACE FUNCTION getSubscribes(i bigint) RETURNS table (id bigint)
    LANGUAGE plpgsql
    AS $$
	
    BEGIN
		return Query
		SELECT user2 FROM subscription WHERE "user1" = i AND "subtype" = '0';
		
	END;
    $$;



